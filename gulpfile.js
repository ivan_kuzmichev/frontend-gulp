'use strict';

global.$ = {
    /**----- Основной таск -----**/
    gulp: require('gulp'),
    sass: require('gulp-sass'),
    postcss: require('gulp-postcss'),
    nunjucks: require('gulp-nunjucks-render'),
    browserSync: require('browser-sync'),
    notify: require('gulp-notify'),
    /**----- Сборка проекта -----**/
    useref: require('gulp-useref'),
    del: require('del'),
    babel: require('gulp-babel'),
    uglify: require('gulp-uglifyjs'),
    tinypng: require('gulp-tinypng-compress'),
    imagemin: require('gulp-imagemin'),
    embedSvg: require('gulp-embed-svg'),
    responsive: require('gulp-responsive'),
	htmlmin: require('gulp-htmlmin'),
	gcmq: require('gulp-group-css-media-queries'),
    cleancss: require('gulp-clean-css'),
    critical: require('critical').stream,
    /**----------- SEO -----------**/
    /** npm i gulp-sitemap gulp-robots **/
    // sitemap: require('gulp-sitemap'),
    // robots: require('gulp-robots'),
    /**---------- Прочее ----------**/
    /** npm i gulp-rsync **/
    // rsync: require('gulp-rsync'),
    /** Для использования uncss: npm i uncss */

    path: {
        config: require('./gulp_modules/config.js'),
        settings: require('./gulp_modules/settings.js')
    }
};

$.path.config.forEach(function (taskPath) {
    require(taskPath)();
});


/**----- Основные таски -----**/

$.gulp.task('default', $.gulp.series(
    $.gulp.parallel('nunjucks', 'scss'),
    $.gulp.parallel('watch', 'browser-sync')
));

$.gulp.task('build', $.gulp.series(
    // $.gulp.parallel('clean'),
    $.gulp.parallel('useref'),
    $.gulp.parallel('build-css', 'build-font', 'build-js', 'build-img', 'build-html'),
    $.gulp.parallel('critical', 'resize')
));

// $.gulp.task('seo', $.gulp.series('robots', 'sitemap'));