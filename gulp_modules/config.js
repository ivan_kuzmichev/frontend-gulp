module.exports = [
    /**----- Основной таск -----**/
    './gulp_modules/tasks/browser-sync',
    './gulp_modules/tasks/nunjucks',
    './gulp_modules/tasks/scss',
    './gulp_modules/tasks/useref',
    './gulp_modules/tasks/watch',
    /**----- Сборка проекта -----**/
    './gulp_modules/tasks/clean',
    './gulp_modules/tasks/build-css',
    './gulp_modules/tasks/build-font',
    './gulp_modules/tasks/build-js',
    './gulp_modules/tasks/build-img',
    './gulp_modules/tasks/resize',
    './gulp_modules/tasks/build-html',
    './gulp_modules/tasks/critical',
    /**----------- SEO -----------**/
    // './gulp_modules/tasks/robots',
    // './gulp_modules/tasks/sitemap',
    /**---------- Прочее ----------**/
    // './gulp_modules/tasks/rsync'
    // './gulp_modules/tasks/uncss'
]