module.exports = function(){
    $.gulp.task('build-html', function(end){
        return $.gulp.src('app/*.html')
        .pipe($.embedSvg())
        .pipe($.htmlmin({collapseWhitespace: true}))
        .pipe($.gulp.dest('dist/'))
        end();
    });
}