module.exports = function(){
    $.gulp.task('sitemap', function(end) {
        $.gulp.src('dist/*.html', {
                read: false
            })
            .pipe($.sitemap({
                siteUrl: 'http://site'
            }))
            .pipe($.gulp.dest('dist/'));
            end();
    });
}