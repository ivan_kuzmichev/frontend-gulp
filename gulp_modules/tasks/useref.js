module.exports = function(){
    $.gulp.task('useref', function(end){
        return $.gulp.src('app/*.html') 
            .pipe($.useref()).on('error', $.notify.onError(function (error) {
                return "Useref error occurred: " + error.message;
            }))
            .pipe($.gulp.dest('app/'))
            .pipe($.browserSync.reload({stream: true}))
        end();
    });
}