module.exports = function(){
    $.gulp.task('rsync', function() {
        return $.gulp.src('dist/**')
        .pipe($.rsync({
            root: 'dist/',
            hostname: 'username@yousite.com',
            destination: 'yousite/public_html/',
            // include: ['*.htaccess'], // Includes files to deploy
            exclude: '/node_modules', // Excludes files from deploy
            recursive: true,
            archive: true,
            silent: false,
            compress: true
        }))
    });
}