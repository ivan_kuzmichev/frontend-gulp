module.exports = function(){
    $.gulp.task('resize', function(){
        return $.gulp.src(['dist/img/**/*', '!dist/img/**/*@*x*'])
        .pipe($.responsive({
            '*': [{
              height: 600,
              rename: {suffix: '@1x'}
            },{
              height: 600 * 2,
              rename: {suffix: '@2x'}
            },{
              height: 600 * 3,
              rename: {suffix: '@3x'}
            }]
        }))
        .pipe($.gulp.dest('dist/img'))
    });
}