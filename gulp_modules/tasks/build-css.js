module.exports = function(){
    $.gulp.task('build-css', function(end){
        return $.gulp.src('app/css/**/*')
            .pipe($.gcmq())
            .pipe($.cleancss({
                level: { 2: { specialComments: 0 },
                compatibility: 'ie9'
            }}))
            .pipe($.gulp.dest('dist/css'))
            end();
    });
}