module.exports = function(){
    $.gulp.task('build-js', function(end){
        return $.gulp.src('app/js/**/*')
        .pipe($.babel({
            presets: ['@babel/env']
        }))
        .on('error', $.notify.onError(function (error) {
            return "A task babel error occurred: " + error.message;
        }))
        .pipe($.uglify())
        .pipe($.gulp.dest('dist/js'))
        end();
    });
}