module.exports = function(){
	$.gulp.task('critical', function () {
		return $.gulp.src('dist/**/*.html', {base: './'})
			.pipe(
				$.critical({
					base: 'dist/',
					inline: true,
					css: [
						'dist/css/main.css'
					],
					dimensions: [
						{height: 720, width: 1280}
					],
					minify: true,
					timeout: 120000
				})
			)
			.on('error', $.notify.onError(function (error) {
                return "A task critical error occurred: " + error.message;
			}))
			.pipe($.htmlmin({collapseWhitespace: true}))
			.pipe($.gulp.dest('./'));
		});
}