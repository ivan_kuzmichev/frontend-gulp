module.exports = function(){
    $.gulp.task('uncss', function(end){
        return $.gulp.src('dist/css/**/*.css')
            .pipe($.postcss([
                require('uncss')({
                    html: ['index.html'],
                })
            ]))
            .on('error', $.notify.onError(function (error) {
                return "A task postcss error occurred: " + error.message;
            }))
            .pipe($.gulp.dest('dist/clean-css'))
        end();
    });
}