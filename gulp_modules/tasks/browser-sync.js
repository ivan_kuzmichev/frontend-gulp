module.exports = function(){
	$.gulp.task('browser-sync', function(end) {
		$.browserSync({
			server: {
				baseDir: 'app/'
			},
			notify: false,
			// open: false
		})
		end();
	});
}