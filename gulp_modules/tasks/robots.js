module.exports = function(){
    $.gulp.task('robots', function(end) {
        $.gulp.src('dist/index.html')
            .pipe($.robots({
                useragent: 'Googlebot, Yandex ',
                allow: ['dist/'],
                disallow: ['app/']
            }))
            .pipe($.gulp.dest('dist/'));
            end();
    });
}
